#  !/usr/bin/env python
#  -*- coding: utf-8 -*-

import StringIO
import json
import logging
import random
import urllib
import urllib2
import datetime
import collections

# for sending images
from PIL import Image
import multipart

# standard app engine imports
from google.appengine.api import urlfetch
from google.appengine.ext import ndb
import webapp2

STATE = ''

TOKEN = '174704923:AAFMOnS2wPbGO9hQrqdcUhG__SRtNuIaOWs'

BASE_URL = 'https://api.telegram.org/bot' + TOKEN + '/'

API_URL = 'http://188.120.233.65/'


# ================================

class EnableStatus(ndb.Model):
    # key name: str(chat_id)
    enabled = ndb.BooleanProperty(indexed=False, default=False)


# ================================

def setEnabled(chat_id, yes):
    es = EnableStatus.get_or_insert(str(chat_id))
    es.enabled = yes
    es.put()

def getEnabled(chat_id):
    es = EnableStatus.get_by_id(str(chat_id))
    if es:
        return es.enabled
    return False


# ================================

class MeHandler(webapp2.RequestHandler):
    def get(self):
        urlfetch.set_default_fetch_deadline(60)
        self.response.write(json.dumps(json.load(urllib2.urlopen(BASE_URL + 'getMe'))))


class GetUpdatesHandler(webapp2.RequestHandler):
    def get(self):
        urlfetch.set_default_fetch_deadline(60)
        self.response.write(json.dumps(json.load(urllib2.urlopen(BASE_URL + 'getUpdates'))))


class SetWebhookHandler(webapp2.RequestHandler):
    def get(self):
        urlfetch.set_default_fetch_deadline(60)
        url = self.request.get('url')
        if url:
            self.response.write(json.dumps(json.load(urllib2.urlopen(BASE_URL + 'setWebhook', urllib.urlencode({'url': url})))))


class WebhookHandler(webapp2.RequestHandler):
    def post(self):
        urlfetch.set_default_fetch_deadline(60)
        body = json.loads(self.request.body)
        logging.info('request body:')
        logging.info(body)
        self.response.write(json.dumps(body))

        update_id = body['update_id']
        message = body['message']
        message_id = message.get('message_id')
        date = message.get('date')
        text = message.get('text')
        fr = message.get('from')
        chat = message['chat']
        chat_id = chat['id']

        global STATE

        if not text:
            logging.info('no text')
            return

        def reply(msg=None, img=None, reply_markup=None):
            if msg:
                resp = urllib2.urlopen(BASE_URL + 'sendMessage', urllib.urlencode({
                    'chat_id': str(chat_id),
                    'text': msg,
                    'disable_web_page_preview': 'true',
                    'reply_markup': reply_markup,
                })).read()
            elif img:
                resp = multipart.post_multipart(BASE_URL + 'sendPhoto', [
                    ('chat_id', str(chat_id)),
                    ('reply_to_message_id', str(message_id)),
                ], [
                    ('photo', 'image.jpg', img),
                ])
            else:
                logging.error('no msg or img specified')
                resp = None

            logging.info('send response:')
            logging.info(resp)

        def getUsers():
            url = API_URL + '?getUsers=GET'
            response = urllib2.urlopen(url)
            data = json.load(response)
            users = {}
            for user in data:
                name = str((user['Name']).encode('utf8'))
                users.update({user['Id'] : name})
            return users

        def getTobaccos(brand):
            if brand is not None:
                url = API_URL + '?getTobaccos=' + brand
                response = urllib2.urlopen(url)
                data = json.load(response)
                tobaccos = []
                for tobacco in data:
                    taste = str((tobacco['Taste']).encode('utf8'))
                    tobaccos.append(taste)
                return tobaccos
            else:
                url = API_URL + '?getTobaccos=GET'
                response = urllib2.urlopen(url)
                data = json.load(response)
                tobaccos = []
                for tobacco in data:
                    name = str((tobacco['Name']).encode('utf8'))
                    taste = str((tobacco['Taste']).encode('utf8'))
                    tobaccos.append(name + ' ' + taste)
                return tobaccos

        def getTodayUser(day):
            url = API_URL + '?getSchedule=' + str(day + 1)
            response = urllib2.urlopen(url)
            data = json.load(response)
            users = getUsers()
            usernames = []
            for user in data:
                if user['UserId'] in users.keys():
                    usernames.append(users[user['UserId']])
            return usernames

        def getWorkHours(day):
            url = API_URL + '?getWorkHours=' + str(day + 1)
            response = urllib2.urlopen(url)
            data = json.load(response)
            workhours = {}
            for oneDay in data:
                if oneDay['Day'] == str(day + 1):
                    workhours.update({'TimeOpen' : oneDay['TimeOpen']})
                    workhours.update({'TimeClose' : oneDay['TimeClose']})
            return workhours

        if text.startswith('/'):
            if '/start' in text:
                reply_keyboard = [['Кто сегодня на смене?', 'Что по табакам?'], ['Есть свободные столики?']]
                reply_markup = {'keyboard': reply_keyboard, 'resize_keyboard': False, 'one_time_keyboard': False}
                reply('Bot enabled', reply_markup=json.dumps(reply_markup))
                setEnabled(chat_id, True)
            elif '/stop' in text:
                reply('Bot disabled')
                setEnabled(chat_id, False)
            elif '/image' in text:
                img = Image.new('RGB', (512, 512))
                base = random.randint(0, 16777216)
                pixels = [base+i*j for i in range(512) for j in range(512)]  # generate sample image
                img.putdata(pixels)
                output = StringIO.StringIO()
                img.save(output, 'JPEG')
                reply(img=output.getvalue())
            elif text.startswith('/tobacco'):
                if (len(text.split(' ')) <= 1):
                    tobaccos = getTobaccos(None)
                    brands = []
                    for t in tobaccos:
                        brand = t.split(' ')[0]
                        if brand not in brands:
                            brands.append(brand)
                    tobaccosString = '; '.join(brands)
                    reply('В настоящий момент есть следующие табаки: ' + tobaccosString
                          + ' Чтобы посмотреть вкусы конкретного табака, введите команду \'/tobaccos *название табака*\'')
                else:
                    tobacco = text.split(' ')[1]
                    tobaccos = getTobaccos(tobacco)
                    for t in tobaccos:
                        reply(t)
                    STATE = ''
            elif text.startswith('/today'):
                day = datetime.datetime.today().weekday()
                users = getTodayUser(day)
                for user in users:
                    reply(user)
            elif text.startswith('/tables'):
                reply('Хз, друг, позвони нам по номеру 960-260 или 89223967886')
            elif text.startswith('/workhours'):
                day = datetime.datetime.today().weekday()
                workhours = getWorkHours(day)
                reply('Сегодня открываемся в ' + str(workhours['TimeOpen']) + ', закрываемся в ' + str(workhours['TimeClose']) + ', успевай!')
            else:
                reply('Ой, вы обращаетесь ко мне? К сожалению, я вас не очень понял, введите \'/\', чтобы увидеть список моих команд!')

        # CUSTOMIZE FROM HERE

        elif (STATE == 'getTobacco'):
            tobaccos = getTobaccos(str(text.encode('utf-8')))
            reply_keyboard = [['Кто сегодня на смене?', 'Что по табакам?'], ['Есть свободные столики?']]
            reply_markup = {'keyboard': reply_keyboard, 'resize_keyboard': False, 'one_time_keyboard': True}
            for tobacco in tobaccos:
                reply(tobacco, reply_markup=json.dumps(reply_markup))
            STATE = ''
        elif str(text.encode('utf-8')) == 'Кто сегодня на смене?':
            day = datetime.datetime.today().weekday()
            users = getTodayUser(day)
            for user in users:
                reply(user)
        elif str(text.encode('utf-8')) == 'Что по табакам?':
            STATE = 'getTobacco'
            tobaccos = getTobaccos(None)
            reply_keyboard = []
            brands = []
            for tobacco in tobaccos:
                brand = tobacco.split(' ')[0]
                if brand not in brands:
                    brands.append(brand)
            reply_keyboard.append(brands)
            reply_markup = {'keyboard': reply_keyboard, 'resize_keyboard': False, 'one_time_keyboard': True}
            reply('Выберите фирму', reply_markup=json.dumps(reply_markup))
        elif str(text.encode('utf-8')) == 'Есть свободные столики?':
            reply('Хз, друг, позвони нам по номеру 960-260')
        else:
            reply('Увы, такой команды нет.')


app = webapp2.WSGIApplication([
    ('/me', MeHandler),
    ('/updates', GetUpdatesHandler),
    ('/set_webhook', SetWebhookHandler),
    ('/webhook', WebhookHandler),
], debug=True)
